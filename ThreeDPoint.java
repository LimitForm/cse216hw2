/**
 * An unmodifiable point in the three-dimensional space. The coordinates are specified by exactly three doubles (its
 * <code>x</code>, <code>y</code>, and <code>z</code> values).
 */

public class ThreeDPoint implements Point {

double x;
double y;
double z;	
	
    public ThreeDPoint(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * @return the (x,y,z) coordinates of this point as a <code>double[]</code>.
     */
    @Override
    public double[] coordinates() {
        double[] loadedCoordinates = new double[3];
        loadedCoordinates[0] = x;
        loadedCoordinates[1] = y;
        loadedCoordinates[2] = z;
    	return loadedCoordinates; 
    }
}
